package com.cognitoauthentication.service;

import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.model.*;
import com.cognitoauthentication.configuration.AwsCognitoConfiguration;
import com.cognitoauthentication.dto.AuthenticationRequest;
import com.cognitoauthentication.dto.SignUpRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class AwsCognitoService {

    @Autowired
    AwsCognitoConfiguration awsCognitoConfiguration;

    public UserType signUp(SignUpRequest signUpRequest) {

        AWSCognitoIdentityProvider cognitoClient = awsCognitoConfiguration.getAmazonCognitoIdentityClient();
        AdminCreateUserRequest cognitoRequest = new AdminCreateUserRequest()
                .withUserPoolId(awsCognitoConfiguration.getUserPoolId())
                .withUsername(signUpRequest.getUsername())
                .withUserAttributes(
                        new AttributeType()
                                .withName("email")
                                .withValue(signUpRequest.getEmail()),
                        new AttributeType()
                                .withName("name")
                                .withValue(signUpRequest.getName()),
                        new AttributeType()
                                .withName("family_name")
                                .withValue(signUpRequest.getLastName()),
                        new AttributeType()
                                .withName("phone_number")
                                .withValue(signUpRequest.getPhoneNumber()),
                        new AttributeType()
                                .withName("email_verified")
                                .withValue("true"))
                .withTemporaryPassword("TEMPORARY_PASSWORD")
                .withMessageAction("SUPPRESS")
                .withDesiredDeliveryMediums(DeliveryMediumType.EMAIL)
                .withForceAliasCreation(Boolean.FALSE);

        AdminCreateUserResult createUserResult = cognitoClient.adminCreateUser(cognitoRequest);
        UserType cognitoUser = createUserResult.getUser();

        return cognitoUser;

    }

    public AuthenticationResultType signIn(AuthenticationRequest authenticationRequest) throws Exception {
        AuthenticationResultType authenticationResult = null;
        AWSCognitoIdentityProvider cognitoClient = awsCognitoConfiguration.getAmazonCognitoIdentityClient();

        final Map<String, String> authParams = new HashMap<>();
        authParams.put("USERNAME", authenticationRequest.getUsername());
        authParams.put("PASSWORD", authenticationRequest.getPassword());

        final AdminInitiateAuthRequest authRequest = new AdminInitiateAuthRequest();
        authRequest.withAuthFlow(AuthFlowType.ADMIN_NO_SRP_AUTH)
                .withClientId(awsCognitoConfiguration.getClientId())
                .withUserPoolId(awsCognitoConfiguration.getUserPoolId())
                .withAuthParameters(authParams);
        AdminInitiateAuthResult result = cognitoClient.adminInitiateAuth(authRequest);
        //Has a Challenge
        if (StringUtils.isNotBlank(result.getChallengeName())) {
        //If the challenge is required new Password validates if it has the new password variable.
            if ("NEW_PASSWORD_REQUIRED".equals(result.getChallengeName())) {
                if (null == authenticationRequest.getNewPassword()) {
                    throw new Exception();
                } else {
                    //we still need the username
                    final Map<String, String> challengeResponses = new HashMap<>();
                    challengeResponses.put("USERNAME", authenticationRequest.getUsername());
                    challengeResponses.put("PASSWORD", authenticationRequest.getPassword());
                    //add the new password to the params map
                    challengeResponses.put("NEW_PASSWORD", authenticationRequest.getNewPassword());
                    //populate the challenge response
                    final AdminRespondToAuthChallengeRequest request =
                            new AdminRespondToAuthChallengeRequest();
                    request.withChallengeName(ChallengeNameType.NEW_PASSWORD_REQUIRED)
                            .withChallengeResponses(challengeResponses)
                            .withClientId(awsCognitoConfiguration.getClientId())
                            .withUserPoolId(awsCognitoConfiguration.getUserPoolId())
                            .withSession(result.getSession());

                    AdminRespondToAuthChallengeResult resultChallenge =
                            cognitoClient.adminRespondToAuthChallenge(request);
                    authenticationResult = resultChallenge.getAuthenticationResult();
                }
            } else {
                //has another challenge
                throw new Exception();
            }
        } else {
            //Doesn't have a challenge
            authenticationResult = result.getAuthenticationResult();
        }
        cognitoClient.shutdown();
        return authenticationResult;
    }
}

package com.cognitoauthentication.configuration;

import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProviderClientBuilder;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Data
@Configuration
@ConfigurationProperties(prefix = "cognito")
public class AwsCognitoConfiguration {

    private String clientId;
    private String userPoolId;
    private String endpoint;
    private String region;
    private String identityPoolId;

    /**
    clientId = 2vmvj11rnrmm3j8j4k8ljl2v63
    userPoolId = eu-central-1_MfP8WXNwY
    endpoint = cognito-idp.eu-central-1.amazonaws.com
    region = eu-central-1
    identityPoolId = eu-central-1:fb3c6846-0b8c-48fa-9cbd-4bf7420d6055
    **/

    @Bean
    public AWSCognitoIdentityProvider getAmazonCognitoIdentityClient() {
        ClasspathPropertiesFileCredentialsProvider propertiesFileCredentialsProvider =
                new ClasspathPropertiesFileCredentialsProvider();

        return AWSCognitoIdentityProviderClientBuilder.standard()
                .withCredentials(propertiesFileCredentialsProvider)
                .withRegion(Regions.EU_CENTRAL_1)
                .build();

    }
}

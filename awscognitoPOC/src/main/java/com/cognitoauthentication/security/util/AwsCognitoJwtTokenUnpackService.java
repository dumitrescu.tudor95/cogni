package com.cognitoauthentication.security.util;

import com.amazonaws.services.cloudtrail.model.InvalidTokenException;
import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.RSAKeyProvider;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.util.StringUtils;

import java.security.interfaces.RSAPublicKey;

public class AwsCognitoJwtTokenUnpackService {

    private final AwsCognitoRSAKeyProvider rsaKeyProvider;

    public AwsCognitoJwtTokenUnpackService(AwsCognitoRSAKeyProvider rsaKeyProvider) {
        this.rsaKeyProvider = rsaKeyProvider;
    }

    public Claims unpack(String token) {
        Claims claims;
        try {
            String keyId = JWT.decode(token).getKeyId();

            claims = Jwts.parser()
                    .setSigningKey(rsaKeyProvider.getPublicKeyById(keyId))
                    .parseClaimsJws(token)
                    .getBody();
        } catch (Exception e) {
            throw new InvalidTokenException("Invalid token");
        }

        String userId = claims.get("username", String.class);
        if (StringUtils.isEmpty(userId) || claims.getExpiration() == null) {
            throw new InvalidTokenException("Invalid Token");
        }

        return claims;
    }
}

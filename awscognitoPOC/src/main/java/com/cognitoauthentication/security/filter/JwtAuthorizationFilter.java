package com.cognitoauthentication.security.filter;

import com.amazonaws.services.cloudtrail.model.InvalidTokenException;
import com.cognitoauthentication.security.util.AwsCognitoJwtTokenUnpackService;
import com.cognitoauthentication.security.util.AwsCognitoRSAKeyProvider;
import io.jsonwebtoken.Claims;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class JwtAuthorizationFilter extends BasicAuthenticationFilter {

    private static final String RSA_KEY_URL = "https://cognito-idp.eu-central-1.amazonaws.com/eu-central-1_MfP8WXNwY/.well-known/jwks.json";

    public JwtAuthorizationFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain filterChain) throws IOException, ServletException {
        UsernamePasswordAuthenticationToken authentication = null;
        authentication = getAuthentication(request);
        if (authentication == null) {
            filterChain.doFilter(request, response);
        }

        SecurityContextHolder.getContext().setAuthentication(authentication);
        filterChain.doFilter(request, response);
    }

    private String extractToken(String token) {
        if (StringUtils.isNotEmpty(token) && token.startsWith("Bearer ")) {
            return token.substring("Bearer ".length());
        } else throw new InvalidTokenException("Invalid token");
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request)  {
        final AwsCognitoRSAKeyProvider rsaKeyProvider = new AwsCognitoRSAKeyProvider(RSA_KEY_URL);
        final AwsCognitoJwtTokenUnpackService cognitoJwtTokenUnpackService = new AwsCognitoJwtTokenUnpackService(rsaKeyProvider);
        final String authorizationHeader = request.getHeader("Authorization");
        final Claims claims;
        try {
            final String token = extractToken(authorizationHeader);
            claims = cognitoJwtTokenUnpackService.unpack(token);
        } catch (InvalidTokenException e) {
            return null;
        }
        Set grantedAuthorities = (Set) ((List)claims.get("cognito:groups"))
                .stream()
                .map(group -> new SimpleGrantedAuthority("ROLE_" + group.toString().toUpperCase()))
                .collect(Collectors.toSet());
        UserDetails userDetails = new User((String) claims.get("username"), "", grantedAuthorities);
        return new UsernamePasswordAuthenticationToken(userDetails, claims, grantedAuthorities);
    }
}

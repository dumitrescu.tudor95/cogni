package com.cognitoauthentication.dto;

import lombok.Data;

@Data
public class SignUpRequest {

    private String username;
    private String name;
    private String lastName;
    private String email;
    private String phoneNumber;
    private String companyName;
    private String companyPosition;

}

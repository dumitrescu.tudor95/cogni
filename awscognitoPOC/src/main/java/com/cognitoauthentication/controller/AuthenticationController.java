package com.cognitoauthentication.controller;

import com.amazonaws.services.cognitoidp.model.AuthenticationResultType;
import com.amazonaws.services.cognitoidp.model.UserType;
import com.cognitoauthentication.dto.AuthenticationRequest;
import com.cognitoauthentication.dto.SignUpRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.cognitoauthentication.service.AwsCognitoService;

@RestController
public class AuthenticationController {

    @Autowired
    AwsCognitoService service;

    @PostMapping
    public UserType signUp(@RequestBody SignUpRequest signUpRequest) {
        return service.signUp(signUpRequest);
    }

    @GetMapping
    public AuthenticationResultType signIn(@RequestBody AuthenticationRequest authenticationRequest) throws Exception { return service.signIn(authenticationRequest);}

    @GetMapping("/test")
    @PreAuthorize("ROLE_ADMIN")
    public String hello(){
        return "Hello, world !";
    }

    @GetMapping("/test2")
    public String hello2(){
        return "Hello, world 2 !";
    }
}
